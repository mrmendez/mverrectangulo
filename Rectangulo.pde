class Rectangulo{
   float xp, yp;
   float ancho, alto;
   color pintado;
   
   Rectangulo(float a, float b, float c, float d){
       xp = a;
       yp = b;
       ancho = c;
       alto = d;
       pintado = color(255,255,255);
       rectMode(CORNER); // ESQUINA SUPERIOR IZQUIERDA CORRESPONDE A (XP, YP).
   }
   
   
   void dibujarRectangulo(){
       fill(pintado);
       rect(xp, yp, ancho, alto);       
   }
   
   boolean isColision(float a, float b){
       return ( (a >= xp && a <= xp+ancho) && ( b>=yp && b <= yp+alto ));
   }
   
}

